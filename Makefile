###############################################
#      /  /       CHARGED1					  #
#   /        /    --------					  #
#  /          /   gitlab.com/charged1		  #
#  /          /   matrix.to/#/@XBow:envs.net  #
#   /        /    --------					  #
#      /  /       Please read before you run. #
###############################################

# $(DESTDIR) is defined in the PKGBUILD.

all:
	@echo Run \'make install\' to install.

install:
	@mkdir -p $(DESTDIR)/usr/bin
	@cp -p cysinfo.sh $(DESTDIR)/usr/bin/cysinfo
	@chmod 755 $(DESTDIR)/usr/bin/cysinfo

uninstall:
	@rm -rf $(DESTDIR)/usr/bin/cysinfo
