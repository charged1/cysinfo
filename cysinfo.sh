#!/usr/bin/env bash

###############################################
#      /  /       CHARGED1                    #
#   /        /    --------                    #
#  /          /   gitlab.com/charged1         #
#  /          /   matrix.to/#/@XBow:envs.net  #
#   /        /    --------                    #
#      /  /       Please read before you run. #
###############################################

# formatting
bold=$(tput bold) # bold
normal=$(tput sgr0) # not bold
LG='\033[1;32m' # Light green
NC='\033[0m' # No Color

gtkconfdir=$HOME/.config/gtk-3.0/settings.ini

# username@hostname
user=$(printf $USER)
hostname=$(hostnamectl | awk -F ": " '/^ Static hostname: / {print $NF}')

# info
os=$(awk -F "=" '/^NAME/ {print $NF}' /etc/os-release)
host=$(hostnamectl | awk -F ": " '/^  Hardware Model: / {print $NF}')
kernel=$(uname -r)
uptime=$(uptime --pretty 2>/dev/null | awk -F "up " '/^up/ {print $NF}')

# Find distro
source /etc/os-release

# Uptime
if [ "$ID" == "nixos" ]; then
    uptime=$(uptime | cut -c 2-9)
fi

# GTK
if test -f "$gtkconfdir"; then
    gtktheme=$(awk -F "=" '/^gtk-theme-name/ {print $NF}' $HOME/.config/gtk-3.0/settings.ini)
    gtkicontheme=$(awk -F "=" '/^gtk-icon-theme/ {print $NF}' $HOME/.config/gtk-3.0/settings.ini)
    gtkfont=$(awk -F "=" '/^gtk-font-name/ {print $NF}' $HOME/.config/gtk-3.0/settings.ini)
fi

# More info
wm=$(printf $DESKTOP_SESSION)
term=$(printf $TERM)
cpu=$(lscpu | awk -F ":                      " '/^Model name:            / {print $NF}' | uniq)

# Shell
shell=$(printf $SHELL | awk -F "/usr/bin/" '/\// {print $NF}')

if [ $shell == "fish" ] ; then
    shellver=$(fish --version | cut -c 15-19)
elif [ $shell == "/bin/bash" ] ; then
    shellver=$(bash --version | awk 'FNR == 1 {print}' | cut -c 19-24)
    shell="bash"
elif [ $shell == "zsh" ] ; then
    shellver=$(zsh --version | cut -c 5-7)
else
    shellver=$()
fi

# RAM
ramused=$(free | awk 'FNR == 2 {print $3}')
ramtotal=$(free | awk 'FNR == 2 {print $2}')
ramusedmb=$(($ramused / 1000))
ramtotalmb=$(($ramtotal / 1000))

# Packaging
# Used to find derivatives' base distro
if [ -v ID_LIKE ]; then
    ID=$ID_LIKE
fi

# printf
printf "${bold}$user@$hostname${normal} \n"
printf "${LG}OS:${NC} $os \n"
printf "${LG}Host:${NC} $host \n"
printf "${LG}Kernel:${NC} $kernel \n"
printf "${LG}Uptime:${NC} $uptime \n"

# Pkg amt
if [ "$ID" == "arch" ]; then
    supported_pkgmgr=true
    pkgamt=$(pacman -Q | wc -l)
    printf "${LG}Packages:${NC} $pkgamt (pacman)"
elif [ "$ID" == "debian" ] || [ "$ID" == "ubuntu debian" ] || [ "$ID" == "ubuntu" ]; then
    supported_pkgmgr=true
    pkgamtxtra=$(dpkg --list | wc --lines)
    pkgamt=$(($pkgamtxtra - 5))
    printf "${LG}Packages:${NC} $pkgamt (dpkg)"
fi

# Flatpak check
if test -f "/usr/bin/flatpak"; then
    flatpaks=$(flatpak list 2>/dev/null | wc --lines)

    if [ $flatpaks -gt 0 ]; then
        printf ", $flatpaks (flatpak)"
    fi
fi

# Snap check
if test -f "/usr/bin/snap"; then
    snaps=$(snap list | wc --lines)
    printf ", $snaps (snap) \n"
else
    printf "\n"
fi

# Back to printing
printf "${LG}Shell:${NC} $shell $shellver \n"

# GTK
if test -f "$gtkconfdir"; then
    printf "${LG}Theme:${NC} $gtktheme [GTK3] \n"
    printf "${LG}Icons:${NC} $gtkicontheme [GTK3] \n"
    printf "${LG}Font:${NC} $gtkfont [GTK 3] \n"
fi

# Back to printing
printf "${LG}Desktop:${NC} $wm \n"
printf "${LG}Terminal:${NC} $term \n"
printf "${LG}CPU:${NC} $cpu \n"
printf "${LG}Memory:${NC} $ramusedmb MB/$ramtotalmb MB \n"
