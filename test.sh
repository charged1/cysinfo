#!/bin/bash

# Run the command in the background
flatpak list >/dev/null 2>&1 &

# Get the PID of the background process
pid=$!

# Wait for the process to complete and get its exit status
wait $pid
status=$?

# Check if the command had an error
if [ $status -ne 0 ]; then
  echo "Error: my_command exited with status $status"
  # Do something if there was an error
else
  echo "Success: my_command exited with status $status"
  # Do something if there was no error
fi
